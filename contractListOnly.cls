public class contractListOnly {
    
    Public List<Contract>contract{get;set;}
    Public string selectedCont {get;set;}
    public contractListOnly(ApexPages.StandardController controller){  
                   contract=[SELECT ContractNumber , AccountId, Status ,StartDate ,EndDate ,ContractTerm 
                       FROM Contract];          
     }
    public contractListOnly(){}
    public PageReference contClicked(){  
          PageReference redirect = new PageReference('/apex/ContractDetailPage'); 
          redirect.getParameters().put('id',selectedCont); 
          redirect.setRedirect(true); 
          String id_Asset = ApexPages.currentPage().getParameters().get('id');
          system.debug('my id for the record is:' +id_Asset);
        
       return redirect;
    }
           
}